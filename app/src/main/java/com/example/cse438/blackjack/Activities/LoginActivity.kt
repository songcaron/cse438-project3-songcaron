package com.example.cse438.blackjack.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.cse438.blackjack.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        login_button.setOnClickListener {
            val email = email_login.text.toString()
            val password = password_login.text.toString()

            Log.d("MainActivity", "Tried to login with email: $email/***")

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                Log.d("Main", "Successfully logged with uid: ${it.result!!.user.uid}")

            }
                .addOnFailureListener {
                    Log.d("Main", "Failed log in: ${it.message}")
                    Toast.makeText(this, "Account doesn't exist: ${it.message}", Toast.LENGTH_SHORT).show()
                }


            back_registration_textview.setOnClickListener{
                Log.d("MainActivity", "Back to registration")

                //launch login activity
                val intent= Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
        }
    }
}