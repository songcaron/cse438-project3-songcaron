package com.example.cse438.blackjack.Activities

import android.graphics.Path
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import com.example.cse438.blackjack.LeaderboardAdapter
import com.example.cse438.blackjack.Player
import com.example.cse438.blackjack.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_game.*


class LeaderboardActivity : AppCompatActivity() {

    var listView: ListView? = null
    var playerList = ArrayList<Player>()
    var adapter: LeaderboardAdapter? = null
    private lateinit var database: DatabaseReference
    val uid = FirebaseAuth.getInstance().uid ?: ""
    private val email = FirebaseAuth.getInstance().currentUser!!.email ?: ""
    private val player: MutableList<Player> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)

        listView = findViewById(R.id.listView) as ListView

        adapter = LeaderboardAdapter(this, playerList)
        (listView as ListView).adapter = adapter

        preparePlayerData()

        // Click event for single list row
        (listView as ListView).onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(
                applicationContext,
                playerList?.get(i)?.email,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun preparePlayerData() {
        database = FirebaseDatabase.getInstance().reference

        val leaderboard = database.child("users").child(uid).orderByChild("wins")

        val leaderboardListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                player.clear()
                dataSnapshot.children.mapNotNullTo(player) {
                    it.getValue<Player>(Player::class.java)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }
        database.child("users").addListenerForSingleValueEvent(leaderboardListener)
        while (player.size != 0) {
            for (i in 1..player.size) {
                playerList.add(player[i])

                }
            }

        }


    }



