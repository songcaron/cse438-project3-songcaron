package com.example.cse438.blackjack.Activities

import android.annotation.TargetApi
import android.content.Context
import android.graphics.drawable.Drawable
import android.media.Image
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.View
import android.widget.ImageView
import com.example.cse438.blackjack.R
import com.example.cse438.blackjack.util.CardRandomizer
import kotlinx.android.synthetic.main.activity_game.*
import java.util.Random
import android.content.res.Resources
import android.support.v4.view.GestureDetectorCompat
import com.example.cse438.blackjack.Player
import com.google.firebase.auth.FirebaseAuth
import android.view.GestureDetector
import android.view.MotionEvent
import android.animation.ObjectAnimator
import android.animation.AnimatorSet
import kotlinx.android.synthetic.main.activity_main.*
import android.view.DragEvent
import android.content.ClipData
import android.content.ClipDescription
import android.content.Intent
import android.view.View.OnDragListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class GameActivity : AppCompatActivity() {


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    val uid = FirebaseAuth.getInstance().uid ?: ""
    private val email = FirebaseAuth.getInstance().currentUser!!.email ?:""
    private var wins: Int = 0
    private var losses: Int = 0
    private lateinit var database: DatabaseReference

    private var playerCards = ArrayList<ImageView>()
    private var dealerCards = ArrayList<ImageView>()
    private lateinit var cardList: ArrayList<Int>
    private var player = Player(uid, email, wins, losses)
    private var dealer = Player("uid", "email", 0, 0)


    //animations and gestures
    private lateinit var mDetector: GestureDetectorCompat
    private var height: Int = 0
    private var width: Int = 0
    private lateinit var deckView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.cse438.blackjack.R.layout.activity_game)

        playerCards = arrayListOf(playerCard1, playerCard2, playerCard3, playerCard4, playerCard5)
        dealerCards = arrayListOf(dealerCard1, dealerCard2, dealerCard3, dealerCard4, dealerCard5)
        cardList = CardRandomizer().getIDs(this)

        //animation and gestures initialization here
        //mDetector = GestureDetectorCompat(this, MyGestureListener())
        var count: Int = 0
        var dealerCount: Int = 0
        var storeFirstCardID: Int = 0

        val savedWins = savedInstanceState?.getInt(winCount.text.toString()) ?: player.wins
        val savedLosses = savedInstanceState?.getInt(lossCount.text.toString()) ?: player.losses

        for(i in 0.. 3){
            //provided from the lab pdf

            val randomizer: CardRandomizer = CardRandomizer()
            val rand: Random = Random()
            val r: Int = rand.nextInt(cardList.size)
            val id: Int = cardList.get(r)
            val name: String = resources.getResourceEntryName(id)

            if(i != 1 && i != 3){

                val playerCardValue: Int = getCardValue(name)
                player.addCardToHand(name)
                score.text = "Score: " + player.getTotalScore(playerCardValue)
                playerCards[count].setImageResource(id)
            }
            else{

                val dealerCardValue: Int = getCardValue(name)
                dealer.addCardToHand(name)
                dealer.getTotalScore(dealerCardValue)

                if(count == 0){
                    dealerCards[count].setImageResource(R.drawable.back)
                    storeFirstCardID = id
                }
                else
                    dealerCards[count].setImageResource(id)
                    count++
                    dealerCount++

            }
            cardList.remove(id)
        }
        //stand and double tap
        stand.setOnClickListener{
            while(dealer.getTotalScore(0) < 17){
                val randomizer: CardRandomizer = CardRandomizer()
                val rand: Random = Random()
                val r: Int = rand.nextInt(cardList.size)
                val id: Int = cardList.get(r)
                val name: String = resources.getResourceEntryName(id)

                cardList.remove(id)
                dealer.addCardToHand(name)
                val dealerCardValue = getCardValue(name)
                dealer.getTotalScore(dealerCardValue)
                dealerCards[dealerCount].setImageResource(id)
                dealerCount++
            }
            dealerCards[0].setImageResource(storeFirstCardID)
            winConditions()
        }

        //hit and right swipe
        hit.setOnClickListener {
            val randomizer: CardRandomizer = CardRandomizer()
            val rand: Random = Random()
            val r: Int = rand.nextInt(cardList.size)
            val id: Int = cardList.get(r)
            val name: String = resources.getResourceEntryName(id)

            cardList.remove(id)
            player.addCardToHand(name)
            val playerCardValue = getCardValue(name)
            score.text = "Score: " + player.getTotalScore(playerCardValue)
            playerCards[count].setImageResource(id)
            count++
            if(player.getTotalScore(0) > 21){
                score.text = "Player loses"
            }

        }
        restartGame.setOnClickListener {
            val intent= Intent(this, GameActivity::class.java)

            startActivity(intent)

        }

        leaderboard.setOnClickListener {
            val intent = Intent(this, LeaderboardActivity::class.java)
            startActivity(intent)
        }

    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState.apply {
            putInt(winCount.text.toString(), player.wins)
            putInt(lossCount.text.toString(), player.losses)

        })
    }

    override fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    fun winConditions(){
        if(player.getTotalScore(0) == 21){
            score.text = "Player wins"
            player.wins++
        }
        else if(player.getTotalScore(0) < 21 && (player.getTotalScore(0) > dealer.getTotalScore(0))){
            score.text = "Player wins"
            player.wins++
        }
        else if(dealer.getTotalScore(0) > 21){
            score.text = "Player wins"
            player.wins++
        }
        else if(dealer.getTotalScore(0) == 21){
            score.text = "Dealer wins"
            player.losses++
        }
        else if(dealer.getTotalScore(0) < 21 && (dealer.getTotalScore(0) > player.getTotalScore(0))){
            score.text = "Dealer wins"
            player.losses++
        }
        else if(player.getTotalScore(0) > 21){
            score.text = "Dealer wins"
            losses++
        }
        else if(dealer.getTotalScore(0) == player.getTotalScore(0)){
            score.text = "Tie"
        }

        winCount.text = "Wins: " + player.wins
        lossCount.text = "Losses: " + player.losses
        saveScoreToDatabase()
    }

    fun saveScoreToDatabase(){
        val uid = FirebaseAuth.getInstance().uid ?: ""
//        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
//
//        val user = Player("uid", "email_register.text.toString()", player.wins, player.losses)
//        ref.setValue(user)
        database = FirebaseDatabase.getInstance().reference

        database.child("users").child(uid).child("wins").setValue(player.wins)
        database.child("users").child(uid).child("losses").setValue(player.losses)

    }

    fun Context.resIdByName(resIdName: String?, resType: String): Int {
        resIdName?.let {
            return resources.getIdentifier(it, resType, packageName)
        }
        throw Resources.NotFoundException()
    }

    private fun restartGame(){
        finish()
        startActivity(intent)
    }


    fun getCardValue(card: String):Int{
        when(card){
            "clubs10" -> return 10
            "clubs2" -> return 2
            "clubs3" -> return 3
            "clubs4" -> return 4
            "clubs5" -> return 5
            "clubs6" -> return 6
            "clubs7" -> return 7
            "clubs8" -> return 8
            "clubs9" -> return 9
            "clubs_ace" -> return 1
            "clubs_jack" -> return 10
            "clubs_king" -> return 10
            "clubs_queen" -> return 10
            "diamonds10" -> return 10
            "diamonds2" -> return 2
            "diamonds3" -> return 3
            "diamonds4" -> return 4
            "diamonds5" -> return 5
            "diamonds6" -> return 6
            "diamonds7" -> return 7
            "diamonds8" -> return 8
            "diamonds9" -> return 9
            "diamonds_ace" -> return 1
            "diamonds_jack" -> return 10
            "diamonds_king" -> return 10
            "diamonds_queen" -> return 10
            "hearts10" -> return 10
            "hearts2" -> return 2
            "hearts3" -> return 3
            "hearts4" -> return 4
            "hearts5" -> return 5
            "hearts6" -> return 6
            "hearts7" -> return 7
            "hearts8" -> return 8
            "hearts9" -> return 9
            "hearts_ace" -> return 1
            "hearts_jack" -> return 10
            "hearts_king" -> return 10
            "hearts_queen" -> return 10
            "spades10" -> return 10
            "spades2" -> return 2
            "spades3" -> return 3
            "spades4" -> return 4
            "spades5" -> return 5
            "spades6" -> return 6
            "spades7" -> return 7
            "spades8" -> return 8
            "spades9" -> return 9
            "spades_ace" -> return 1
            "spades_jack" -> return 10
            "spades_king" -> return 10
            "spades_queen" -> return 10
            else -> return 0
        }
    }

//    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {
//
//        private var swipedistance = 150
//
//
//        override fun onDoubleTap(e: MotionEvent?): Boolean {
//
//            moveTo(-this@GameActivity.width / 2f + deck.width / 2, this@GameActivity.height / 2f - deck.height / 2f)
//            return true
//        }
//
//        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
//          if (e2.x - e1.x > swipedistance) { //swipe right!
//                moveTo(this@GameActivity.width / 2f - deck.width / 2, 0f)
//                return true
//            }
//            return false
//        }
//    }
//    fun moveTo(targetX: Float, targetY: Float) {
//
//        val animSetXY = AnimatorSet()
//
//        val x = ObjectAnimator.ofFloat(
//            deckView,
//            "translationX",
//            deckView.translationX,
//            targetX
//        )
//
//        val y = ObjectAnimator.ofFloat(
//            deckView,
//            "translationY",
//            deckView.translationY,
//            targetY
//        )
//
//        animSetXY.playTogether(x, y)
//        animSetXY.duration = 300
//        animSetXY.start()
//
//
//
//    }

}
