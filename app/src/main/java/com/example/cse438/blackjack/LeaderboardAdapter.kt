package com.example.cse438.blackjack

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.cse438.blackjack.Activities.MainActivity

class LeaderboardAdapter(private val activity: Activity, list: List<Player>) : BaseAdapter() {

    private var playerList = ArrayList<Player>()

    init {
        this.playerList = playerList as ArrayList
    }

    override fun getCount(): Int {
        return playerList.size
    }

    override fun getItem(i: Int): Any {
        return i
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    @SuppressLint("InflateParams", "ViewHolder")
    override fun getView(i: Int, convertView: View?, viewGroup: ViewGroup): View {
        var vi: View = convertView as View
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        vi = inflater.inflate(R.layout.player_item, null)
        val playerName = vi.findViewById<TextView>(R.id.name)
        val playerWin = vi.findViewById<TextView>(R.id.wins)
        val playerLoss = vi.findViewById<TextView>(R.id.losses)
        val playerScoreRatio = vi.findViewById<TextView>(R.id.score)
        playerName.text = playerList[i].email
        playerWin.text = playerList[i].wins.toString()
        playerLoss.text = playerList[i].losses.toString()
        playerScoreRatio.text = (playerList[i].wins/playerList[i].losses).toString()
        return vi
    }
}