In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
Gestures and the leaderboard didn't work. I wasn't sure how to call doubletap/swipe because they were in a private class. I couldn't find a way
to retrieve data from firebase database, so I wasn't sure on how to update the leaderboard.

* Is there anything that you did that you feel might be unclear? Explain it here.
Registration and login work perfectly and everything is recorded in firebase database.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?
Didn't have time to implement the creative portion, unfortunately.
